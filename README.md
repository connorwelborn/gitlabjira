# GitLabJira

Demo of GitLab Jira Integration

## Table of contents
1. [How To Set Up](https://gitlab.com/connorwelborn/gitlabjira#how-to-set-up)
2. [How To View Jira Issues Without Leaving GitLab](https://gitlab.com/connorwelborn/gitlabjira#how-to-view-jira-issues-without-leaving-gitlab)
3. [How To Reference Jira Issues From GitLab Commits](https://gitlab.com/connorwelborn/gitlabjira#how-to-reference-jira-issues-from-gitlab-commits)

## How To Set Up

Follow these steps to connect a GitLab repository to a Jira project.

### Step 1

On Jira, click "Account Settings" under your user dropdown.

![Step 1](/images/001.jpg "Step 1")

### Step 2

Click "Security".

![Step 2](/images/002.jpg "Step 2")

### Step 3

Click "Create and manage API tokens".

![Step 3](/images/003.jpg "Step 3")

### Step 4

Click "Create API token"

![Step 4](/images/004.jpg "Step 4")

### Step 5

Create a label for your API token, then click "create". This label can be anything you want.

![Step 5](/images/005.jpg "Step 5")

### Step 6

Copy your API token. Paste it somewhere safe; you'll need it later and won't be able to view it again.

![Step 6](/images/006.jpg "Step 6")

### Step 7

Close the API token window.

![Step 7](/images/007.jpg "Step 7")

### Step 8

On GitLab, navigate to "Settings/Integrations" on the sidebar of your repository.

![Step 8](/images/008.jpg "Step 8")

## Step 9

Scroll down until you find "Jira".

![Step 9](/images/009.jpg "Step 9")

### Step 10

Click on "Jira".

![Step 10](/images/010.jpg "Step 10")

### Step 11

In the "Web URL" field, enter the base URL of Jira. For Disney, this is "https://jira.disney.com"
In the "Username or Email" field, enter your email address.
In the "Password or API token" field, enter the API token you copied from Jira.
Scroll down.

![Step 11](/images/011.jpg "Step 11")

### Step 12

Check the box for "Enable Jira issues".
In the "Jira project key" field, enter the project key for the Jira project you are integrating with this repository.
The project key is the series of numbers before a ticket, such as "BCAM" from "BCAM-1000".
Click on "Test settings" to verify your connection.

![Step 12](/images/012.jpg "Step 12")

### Step 13

Wait for the "Connection successful." pop-up in the bottom-left corner.

![Step 13](/images/013.jpg "Step 13")

### Step 14

Click on "Save Changes".

![Step 14](/images/014.jpg "Step 14")

### Step 15

Verify that your Jira settings are listed as active.

![Step 15](/images/015.jpg "Step 15")

## How To View Jira Issues Without Leaving GitLab

You can now view Jira issues from the repository-integrated issue tracker on GitLab.

### Step 1

Navigate to "Issues/Jira Issues" from within your GitLab repository.

![Issues 1](/images/016.jpg "Issues 1")

### Step 2

View the list of all Jira issues tagged with the project key you defined previously.

![Issues 2](/images/017.jpg "Issues 2")

### Example

You can view the Jira issues associate with this repository here:
https://gitlab.com/connorwelborn/gitlabjira/-/integrations/jira/issues

## How To Reference Jira Issues From GitLab Commits

### Step 1

Include the Jira issue idenfitifer (such as "GLJ-1") in the message of your git commit.

![Reference 1](/images/018.jpg "Reference 1")

### Step 2

View your repo's commit history on GitLab and see how the new commit links to the referenced Jira issue.

![Reference 2](/images/019.jpg "Reference 2")

### Step 3

You can also see this reference within the actual commit's page

![Reference 3](/images/020.jpg "Reference 3")

### Step 4

By clicking the name of the Jira issue within the GitLab commit, you will be taken to the Jira issue on Jira proper.

Here you can see that this reference is bi-directional, as Jira has also created links and comments back to GitLab.

![Reference 4](/images/021.jpg "Reference 4")

### Example

You can view a commit implementing this type of reference for this repository here:
https://gitlab.com/connorwelborn/gitlabjira/-/commit/f69bf767fb0dbef507a310b9f67d13f706a93608